<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Berlatih Function PHP</h1>

    <?php

      echo "<h3> Soal No 1 Greetings</h3>";

      function greetings($nama){
               echo "Halo " . $nama . " ,Selamat Datang di Sanbercode! <br>";
      }
               greetings("Ariandi");
               greetings("Yudis");
               greetings("Risky");

      echo "<br>";

      echo "<h3>Soal No 2 Reverse String</h3>";

      function reverse($kata1){
            $panjangKata = strlen($kata1);
            $tampung = "";
            for($i=$panjangKata-1; $i>=0; $i--){
                $tampung .= $kata1[$i];

              }
            return $tampung;
       } 

       function reverseString($kata2){
            $revString = reverse($kata2);
            echo $revString . '<br>';
       }

        reverseString("Ariandi");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");
        echo "<br>";

        echo "<h3>Soal No 3 Palindrome</h3>";

        function palindrome($pali){
            $balikKata = reverse($pali);
            if($pali === $balikKata){
                echo $pali . " => true <br>";
            }else{
                echo $pali . "=> false <br>";
            }
        }

        palindrome("katak");
        palindrome("kok");
        palindrome("now");
        palindrome("racecar");

        echo "<h3>Soal No 4 Tentukan Nilai</h3>";
        function tentukan_nilai($nilai){
            if($nilai >= 85 && $nilai <100){
                return $nilai . " => Sangat Baik <br>";
            }else if($nilai >=70 && $nilai < 85){
                return $nilai . " => Baik <br>";
            }
        }

        echo tentukan_nilai(98);
        echo tentukan_nilai(76);
        echo tentukan_nilai(67);
        echo tentukan_nilai(43);

        ?>
    
    </body>
    </html>
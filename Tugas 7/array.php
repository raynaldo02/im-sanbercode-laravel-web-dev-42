<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Tugas Array 1, 2, 3</h1>

    <?php
    
    $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];

    echo "<pre>";
    print_r($kids);
    echo "</pre>";

    echo "<br>";

    $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];

    echo "<pre>";
    print_r($adults);
    echo "</pre>";
    echo "<br>";

    echo "Data Kids dan Adults : <br>";

    echo "<br>";
    
    echo "Total Kids : " . count($kids) . "<br>";
    echo "<ol>";
    echo "<li>" . $kids[0] . "</li>";
    echo "<li>" . $kids[1] . "</li>";
    echo "<li>" . $kids[2] . "</li>";
    echo "<li>" . $kids[3] . "</li>";
    echo "<li>" . $kids[4] . "</li>";
    echo "<li>" . $kids[5] . "</li>";
    echo "</ol>";

    echo "<br>";
    
    echo "Total Adults : " . count($adults) . "<br>";
    echo "<ol>";
    echo "<li>" . $adults[0] . "</li>";
    echo "<li>" . $adults[1] . "</li>";
    echo "<li>" . $adults[2] . "</li>";
    echo "<li>" . $adults[3] . "</li>";
    echo "<li>" . $adults[4] . "</li>";
    echo "</ol>";

    $bio = [
        ["Name" => "Will Byers","Age" =>  "12", "Aliases" => "Will the Wise", "Status" => "Alive"],
        ["Name" => "Mike Wheeler", "Age" => "12", "Aliases" => "Dungeon Master","Status" => "Alive"],
        ["Name" => "Jim Hopper", "Age" => "43", "Aliases" => "Chief Hopper","Status" => "Deceased"],
        ["Name" => "Eleven", "Age" => "12", "Aliases" => "E1", "Status" =>"Deceased"]
    ];

    echo "<pre>";
    print_r($bio);
    echo "</pre>";

    
   


    ?>

</body>
</html>